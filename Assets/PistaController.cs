﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistaController : MonoBehaviour
{
    public GameObject pista;
    public int Contador; //variable que controla el tiempo que durará una pista en juego
    // Start is called before the first frame update
    void Start()
    {

        Contador = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Contador < 300)
        {
            Contador++;
        }
        else
        {
            Destroy(this.gameObject);//despues de la condicion la pista se borra
        }

    }
}

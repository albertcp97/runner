﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Disparo : MonoBehaviour
{
    public GameObject shoot;
    public GameObject pos;
 
    public int velBala;
    public int timer;
    //Variables privadas
    private Rigidbody thisRigidbody;
    // Start is called before the first frame update
    void Start()
    {   
        shoot = GameObject.Find("shoot");
        pos = GameObject.Find("Square");
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        //Movemos el arma en horizontal
       
           
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(velBala, 0));//damos velocidad a la bala
           

        if (timer < 100)//controlamos su vida en el juego, evitamos que hayan muchas balas
        {
            timer++;
        }
        else
        {
            Destroy(this.gameObject);
        }

        
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Muros" || collision.gameObject.tag == "Murillo") //desaparece al tocar muro o trampa
        {
            

            Destroy(this.gameObject);

        }

    }
    public void OnCollisionExit2D(Collision2D collision)//desaparece al tocar muro o trampa
    {
        if (collision.gameObject.tag == "Muros" || collision.gameObject.tag == "Murillo")
        {
            Debug.Log("Peta");

            Destroy(this.gameObject);

        }

    }
}
